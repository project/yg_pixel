(function ($) {
 Drupal.behaviors.yg_asymmetry = {
   attach: function (context, settings) {
	
	'use strict';
	    $(document).click(function(e) {
		    if (!$(e.target).is('.panel-body')) {
		      $('.collapse').collapse('hide');      
		    }
	    });

		 jQuery(window).scroll(function(){
	          var scroll = $(window).scrollTop();
	          if (scroll >= 10) {
	              $("#main-menu").addClass("sticky");
	          } else {
	              $("#main-menu").removeClass("sticky");
	          }

	          });
		jQuery(document).ready(function( $ ) {	
				$('.portfolio-popup').magnificPopup({
			    type: 'image',
			    removalDelay: 300,
			    mainClass: 'mfp-fade',
			    gallery: {
			      enabled: true
			    },
			    zoom: {
			      enabled: false,
			      duration: 300,
			      easing: 'ease-in-out',
			      opener: function(openerElement) {
			        return openerElement.is('img') ? openerElement : openerElement.find('img');
			      }
			    }

				});
				$(".testimonial-slider").owlCarousel({
				        items:1,
				        itemsDesktop:[1000,1],
				        itemsDesktopSmall:[979,1],
				        itemsTablet:[768,1],
				        pagination: true,
				        autoPlay:true
				    });

				});		

    
				$(window).on('load', function(){
				    var $container = $('.portfolioContainer');
				    $container.masonry({
					  itemSelector: '.categories',
					  percentPosition: true
					});

					$('.portfolio-item .portfolio-popup').magnificPopup({
	                   type: 'image',
	                   removalDelay: 300,
	                   mainClass: 'mfp-fade',
	                   gallery: {
	                       enabled: true
	                   },
	                   zoom: {
	                       enabled: false,
	                       duration: 300,
	                       easing: 'ease-in-out',
	                       opener: function(openerElement) {
	                           return openerElement.is('img') ? openerElement : openerElement.find('img');
	                       }
	                   }
	               });
 				}); 

 	var socialicon = $( ".social-header.clone" ).clone();
    $(".social-header.clone").remove();
    $(".navbar-collapse" ).append(socialicon);
    $(".social-header").removeClass('clone');
	
}}})(jQuery);// End of use strict